﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GridViewModal2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Create new DataTable and DataSource objects.
                DataTable table = new DataTable();

                // Declare DataColumn and DataRow variables.
                DataColumn column;
                DataRow row;
                DataView view;

                // Create new DataColumn, set DataType, ColumnName and add to DataTable.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "id";
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "name";
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "city";
                table.Columns.Add(column);

                // Create new DataRow objects and add to DataTable.
                for (int i = 0; i < 10; i++)
                {
                    row = table.NewRow();
                    row["id"] = i;
                    row["name"] = "item " + i.ToString();
                    row["city"] = "item2 " + i.ToString();
                    table.Rows.Add(row);
                }


                // Create a DataView using the DataTable.
                view = new DataView(table);

                // Set a DataGrid control's DataSource to the DataView.
                GridView1.DataSource = view;
                GridView1.DataBind();
            }

           
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label4.Text = (GridView1.SelectedRow.FindControl("Label1") as Label).Text;
            Label5.Text = (GridView1.SelectedRow.FindControl("Label2") as Label).Text;
            Label6.Text = (GridView1.SelectedRow.FindControl("Label3") as Label).Text;
            
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "MyScript", "$('#Panel2').modal('show');", true);


        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {

        }
    }
}