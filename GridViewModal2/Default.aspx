﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GridViewModal2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
     

<asp:Panel ID="Panel1" runat="server">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="4" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                GridLines="None" class="gvv">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="id">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("city") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("city") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Select" Text="Select" />
                </Columns>
                <FooterStyle BackColor="#990000" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle ForeColor="#333333" BackColor="#FFFBD6" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
            <br />
        </asp:Panel>

    

        <div class="modal" tabindex="-1" role="dialog" ID="Panel2" >
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
            <table >
                <caption class="style4">
                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is your Selected Data</strong></caption>
                <tr>
                    <td >
                        <asp:Label ID="Label7" runat="server" Text="ID:"></asp:Label>
                    </td>
                    <td >
                        &nbsp;
                        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label8" runat="server" Text="Name:"></asp:Label>
                    </td>
                    <td >
                        &nbsp;
                        <asp:Label ID="Label5" runat="server" Text="Label" ></asp:Label>
                    </td>
                    
                </tr>
                <tr>
                    <td >
                        &nbsp;<asp:Label ID="Label9" runat="server" Text="City:"></asp:Label>
                    </td>
                    <td >
                        &nbsp;
                        <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                    </td>
                    
                </tr>
                
            </table>

</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" runat="server" onserverclick="Unnamed_ServerClick">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



        
    <script src="Scripts/jquery-3.4.1.js"></script>
    <script src="Scripts/DataTables/jquery.dataTables.js"></script>
    <script src="Scripts/bootstrap.js"></script>

    <link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="Content/bootstrap.css" rel="stylesheet" />
    
    <script type="text/javascript" charset="utf-8">
        $(function () {
            $(".gvv").prepend($("<thead></thead>").append($(".gvv").find("tr:first"))).dataTable();
            $(".gvv").dataTable();
        });
    </script>

</asp:Content>
